/**
 * This one is a bit convoluted
 *
 * <input type="text" ng-model="model.field" autocomplete data-url="/searchurl" data-field="name" data-labelfn="label_cb()" data-trigger-length="2">
 */
'use strict';
angular.module('common.autocomplete', [])
.directive('autocomplete', ['$timeout', '$http', '$compile', function ($timeout, $http, $compile) {
    return {
        restrict: 'A',
        replace: false,
        require: 'ngModel',
        scope: {
            'labelfn': '&',
            'model': '=ngModel'
        },
        link: function (scope, element, attrs, ngModel) {
            var timer;
            var query_url = attrs.url;
            var label_expr = function () { return '{{ item[field] }}'; };

            if (typeof(attrs.labelfn) !== 'undefined') {
                label_expr = scope.labelfn;
            }

            var popup = $('<div class="autocomplete-popup" ng-show="show_popup"><ul><li ng-repeat="item in items" ng-click="select($index)">'+ label_expr() +'</li><li ng-show="items.length==0">No users found</li></ul></div>');

            scope.field = attrs.field;
            scope.items = [];
            scope.show_popup = false;

            element.after(popup);
            $compile(popup)(scope);

            element.on('keyup', function (event) {
                var text = this.value;
                if (text.length >= attrs.triggerLength) {

                    if (timer) {
                        $timeout.cancel(timer);
                    }
                    timer = $timeout(function () {
                        $http.get(query_url, {params: {search: text, field: scope.field}}).success(function (res) {
                            scope.items = res.items;
                            scope.show_popup = true;
                        });
                    }, 300);
                }
                else {
                    scope.show_popup = false;
                }
            });
            element.on('focus', function (event) {
                element.parents('.with-popup').css('z-index', '40000');
                if (scope.items.length > 0) {
                    scope.show_popup = true;
                    scope.$apply();
                }
            }).on('blur', function (event) {
                element.parents('.with-popup').css('z-index', '1');
                scope.show_popup = false;
            });

            scope.$watch('model', function (newval, oldval) {
                if (newval && newval[scope.field]) {
                    element.val(newval[scope.field]);
                }
            });

            scope.select = function (index) {
                var item = scope.items[index];
                ngModel.$setViewValue(item);
                scope.items = [];
                scope.show_popup = false;
            };
        }
    };
}])
;

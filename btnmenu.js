/**
 * <div class="btnmenu">
 *      Button Text <i class="fa fa-angle-down"></i>
 *      <ul>
 *          <li><a href="#" ng-click="detail(user.id)">Detail</a></li>
 *          <li><hr></li>
 *          <li><a href="/admin/users/{{ user.id }}">Edit</a></li>
 *          <li><hr></li>
 *          <li ng-hide="staff_list"><a href="#" ng-click="confirm_login(user.id)">Login as User</a></li>
 *      </ul>
 *  </div>
 */

'use strict';
angular.module('common.btnmenu', [])
.directive('btnmenu', function () {
    return {
        restrict: 'C',
        transclude: true,
        template: '<div ng-transclude></div>',
        link: function (scope, element, attrs) {
            element.on('click', function (event) {

                var menu = element.find('ul');
                menu.show(100, function () {
                    $(document).on('click', function (event) {
                        if (menu.find(event.target).length == 0) {
                            menu.hide();
                            $(document).off('click');
                            event.stopImmediatePropagation();
                        }
                    });
                });

            });
        }
    };
})
;

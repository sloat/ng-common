/**
 * This one's easy:
 * <textarea class="ckeditor" ng-model="model.content"></textarea>
 */
'use strict';
angular.module('common.ckeditor', [])
.directive('ckeditor', function () {
    return {
        restrict: 'C',
        require: '?ngModel',
        link: function (scope, element, attrs, ngModel) {
            var ck = CKEDITOR.replace(element[0], {
                //config here
                filebrowserBrowseUrl: '/admin/file-browser?type=files',
                filebrowserUploadUrl: '/admin/file-uploader?type=files',
                filebrowserImageBrowseUrl: '/admin/file-browser?type=images',
                filebrowserImageUploadUrl: '/admin/file-uploader?type=images',
                filebrowserWindowWidth: '640',
                filebrowserWindowHeight: '480'
            });

            if (!ngModel) return;

            ck.on('change', function () {
                scope.$apply(function () {
                    ngModel.$setViewValue(ck.getData());
                });
            });
            ck.on('key', function () {
                scope.$apply(function () {
                    ngModel.$setViewValue(ck.getData());
                });
            });

            ngModel.$render = function (value) {
                ck.setData(ngModel.$viewValue);
            };
            ck.once('instanceReady', function (event) {
                ngModel.$render();
            });

            element.bind('$destroy', function () {
                ck.destroy();
            });
        }
    };
})


/**
 * This requires bootstrap-datepicker and moment.js
 *
 *  <div class="date-nav" ng-model="current_date"></div>
 * 
 */

'use strict';
angular.module('common.datenav', [])
.directive('dateNav', function () {
    return {
        restrict: 'C',
        scope: false,
        require: '?ngModel',
        link: function (scope, element, attrs, ngModel) {
            var today = moment().startOf('day');
            element.datepicker({
                format: 'yyyy-mm-dd',
                beforeShowDay: function (date) {
                    return date.valueOf() < today.valueOf() ? 'disabled' : '';
                }
            }).on('changeDate', function (event) {
                ngModel.$setViewValue(event.date);
                scope.$apply();
            });
            ngModel.$render = function () {
                var m = moment(ngModel.$viewValue);
                var today = moment().startOf('day');

                if (m < today)
                    m = today;

                element.datepicker('update', m.format('YYYY-MM-DD'));
            };
        }
    };
})
;

/**
 * Requires Plupload library
 *
 * <file-uploader ng-model="filename" upload-url="/admin/file-upload/slides" file-type="images" multi="false">Browse for image</file-uploader>
 * 
 */
'use strict';
angular.module('common.fileupload', ['ngSanitize'])
.directive('fileUploader', ['$sce', function ($sce) {
    var file_filters = {
        images: {
            title: 'Image Files',
            extensions: 'jpg,jpeg,gif,png'
        },
        docs: {
            title: 'Documents',
            extensions: 'doc,pdf,docx,xlsx,xls'
        }
    };
    return {
        restrict: 'E',
        template: '<div class="widget upload no-label"><button class="small" id="{{ button_id }}" type="button"><i class="fa fa-fw fa-folder"></i> <span ng-transclude></span></button></div>',
        transclude: true,
        replace: true,
        require: '?ngModel',
        scope: {
            'fileType': '@',
            'multi': '=',
            'uploadUrl': '@'
        },
        link: function (scope, element, attrs, ngModel) {
            var multi = scope.multiselect || false;

            scope.button_id = 'upload_button' + scope.$id;

            //Configure plupload object
            var plup = new plupload.Uploader({
                runtimes: 'html5,flash,silverlight',
                browse_button: element[0],
                url: scope.uploadUrl,
                flash_swf_url: '/static/plupload/plupload.flash.swf',
                silverlight_xap_url: '/static/plupload/plupload.silverlight.xap',
                multi_selection: scope.multi,
                filters: [file_filters[scope.fileType]]
            });

            plup.init();

            plup.bind('FileUploaded', function (uploader, file, info) {
                var data = JSON.parse(info.response);
                ngModel.$setViewValue(data.cleanFileName);
                scope.$root.$broadcast('uploader:complete', uploader, file);
                scope.$apply();
            });
            plup.bind('UploadProgress', function (uploader, file) {
                scope.$root.$broadcast('uploader:progress', uploader, file);
            });

            if (!scope.multi) {
                plup.bind('FilesAdded', function (uploader, files) {
                    uploader.start();
                });
            }

            element.bind('$destroy', function () {
                plup.destroy();
            });
        }
    };
}])
;

'use strict';
angular.module('common.filters', [])
.filter('datetime', function () {
    return function (input, format) {
        if (!input || input.length == 0)
            return '';
        var format = format || "MM/DD/YYYY hh:mm a";
        return moment(input).tz('America/New_York').format(format);
    }
})
.filter('filename', function () {
    return function (input) {
        if (input)
            return input.substr(input.lastIndexOf('/') + 1);
    };
})
.filter('yesno', function () {
    return function (input) {
        return input ? "Yes" : "No";
    }
})
.filter('csv', function () {
    return function (input, key) {
        if (!input)
            return '';
        var output = [];
        for (var i = 0; i < input.length; i++) {
            if (key)
                output.push(input[i][key]);
            else
                output.push(input[i]);
        }

        if (output.length > 0)
            return output.join(', ');
        return '';
    }
})
;


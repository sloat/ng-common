/**
 * 
 * <multi-select ng-model="selections" keyfield="'id'" valfield="'name'" items="items"></multi-select>
 *
 */
'use strict';
angular.module('common.multiselect', [])d
.directive('multiSelect', function () {
    return {
        restrict: 'E',
        template: '<ul><li ng-repeat="item in copied_items" ng-click="toggle_item(item)" ng-class="{selected: item._selected}"><i ng-class="{fa: true, \'fa-fw\': true, \'fa-check-square-o\': item._selected, \'fa-square-o\': !item._selected}"></i> {{ item[valfield] }}</li></ul>',
        require: '^ngModel',
        scope: {
            'items': '=',
            'keyfield': '=',
            'valfield': '=',
        },
        link: function (scope, element, attrs, ngModel) {
            scope._ready = false;
            scope.copied_items = false;
            ngModel.$render = render;
            scope.$watch(render);

            scope.$watchCollection('items', function () {
                if (scope.items !== undefined) {
                    scope.copied_items = [];
                    scope.items.forEach(function (item) {
                        var i = angular.extend({}, item);
                        scope.copied_items.push(i);
                    });
                    scope._ready = true;
                }
            });
            
            function render() {
                if (!scope._ready) {
                    return;
                }
                if (ngModel.$viewValue && scope.copied_items) {

                    var mitems = ngModel.$viewValue;
                    mitems.forEach(function (mitem) {
                        scope.copied_items.forEach(function (item) {
                            if (item[scope.keyfield] == mitem) {
                                item._selected = true;
                            }
                        });
                    });
                }
            };

            scope.toggle_item = function (item) {
                item._selected = !item._selected;
                var selected = scope.copied_items.filter(function (item) {
                    return item._selected == true;
                });

                var keys = selected.map(function (item) {
                    return item[scope.keyfield];
                });

                ngModel.$setViewValue(keys);
            };
        }
    };
})

'use strict';
angular.module('common.notify', [])
.factory('Notify', function () {
    return {
        'messages': [],
        'msg': function (message, style) {
            this.messages.push({text: message, type: style});
        },
        'remove': function (index) {
            this.messages.splice(index, 1);
        },
    };
})
.directive('notifications', function () {
    return {
        restrict: 'ACE',
        template: '<ul><li ng-repeat="msg in Notify.messages" ng-class="msg.type" ng-click="remove_msg(msg, $index)">{{ msg.text }}</li></ul>',
        scope: {},
        controller: ['$scope', 'Notify', function ($scope, Notify) {
            $scope.remove_msg = function (msg, index) {
                Notify.remove(index);
            };
        }]
    };
})

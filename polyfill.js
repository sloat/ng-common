/**
 * This almost isn't deserving... but anyway
 * Use with webshims library
 */

'use strict';
angular.module('common.polyfill', [])
.directive('polyfill', function () {
    return {
        restrict: 'C',
        link: function (scope, element, attrs) {
            element.updatePolyfill();
        }
    };
})
;

angular.module('common.services', [])
.config(['$httpProvider', function ($httpProvider) {
    $httpProvider.defaults.headers.common['X-Requested-With'] = 'XMLHttpRequest';
}])
.controller('MainCtrl', ['Title', '$scope', function (Title, $scope) {
    // Global items available to templates
    $scope.Title = Title;
}])
.factory('Flash', ['$rootScope', '$timeout', function ($rootScope, $timeout) {
    //This could be done better
    var messages = [];
    var Flash = {};

    Flash.remove = function (index) {
        try {
            messages.splice(index, 1);
        }
        catch (ex) {
        }
    };
    
    Flash.clear = function () {
        messages = [];
    };

    Flash.msg = function (message, category, timeout) {
        var delay = timeout || 5000;
        var index = messages.length;

        var data = {'text': message, 'category': category, 'promise': null};

        if (delay != 0) {
            var p = $timeout(function () {
                Flash.remove(index);
            }, delay);
            data.promise = p;
        }

        messages.push(data);
        $rootScope.$emit('flash:message', messages);
    };

    return Flash;
}])
.factory('Title', function () {
    var title = 'Default site title';
    return {
        get: function () { return title; },
        set: function (newTitle) { title = newTitle; },
    };
})
.factory('request', ['$http', function ($http) {
    //Wrapper for $http with url pattern replacement
    //Non-matched params are passed to the regular parameter handler
    //
    //request.get('/user/:id', {id: 5, fields: 'name,lastlogin'}).success(...);
    //
    function replace_params(url, params) {
        var query_params = {};
        angular.forEach(params, function (value, key) {
            var test = new RegExp(':' + key + '(?:\\b)', 'g');
            if (test.test(url)) {
                url = url.replace(test, value);
            }
            else {
                query_params[key] = value;
            }
        });
        return {url: url, params: query_params};
    }
    return {
        get: function (url, params) {
            var req = replace_params(url, params);
            return $http.get(req.url, {params: req.params});
        },
        post: function (url, data) {
            return $http.post(url, data);
        },
        put: function (url, data, params) {
            var req = replace_params(url, params);
            return $http.put(req.url, data, {params: req.params});
        },
        delete: function (url, params) {
            var req = replace_params(url, params);
            return $http.delete(req.url, {params: req.params});
        }
    };
}])
.factory('TriggerDelay', ['$timeout', function ($timeout) {
    return {
        set: function (delay, cb) {
            var timer;

            if (timer) {
                $timeout.cancel(timer);
            }
            timer = $timeout(function () {
                cb();
            }, delay);
        }
    };
}])
;

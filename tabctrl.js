/***
 *   <div class="tabctrl">
 *      <div class="tabs">
 *          <a href="#" data-target="#tab1" class="active">Tab1</a>
 *          <a href="#" data-target="#tab2">Tab2</a>
 *      </div>
 *      <section data-tab="#tab1" class="active"></section>
 *      <section data-tab="#tab2"></section>
 *  </div>
 *
 **/
'use strict';
angular.module('common.tabctrl', [])
.directive('tabctrl', function () {
    return {
        restrict: 'C',
        scope: {},
        transclude: true,
        template: '<div ng-transclude></div>',
        controller: function ($scope, $element) {
        },
        link: function (scope, element, attrs) {
            element.on('click', '.tabs a', function (event) {
                var target = $(this).data('target');
                $(this).siblings().removeClass('active');
                $(this).addClass('active');
                $(this).parents('.tabctrl').find('section').removeClass('active');
                $(this).parents('.tabctrl').find('section[data-tab="'+ target +'"]').addClass('active');
                localStorage.lasttab = target;
                event.preventDefault();
            });

            if (localStorage.lasttab) {
                angular.element('.tabs a[data-target="' + localStorage.lasttab + '"]').click();
            }
        }
    };
})
;
